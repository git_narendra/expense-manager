<?php include "expense-header.php"; ?>

 <div class="container-fluid">
 	<div class="row">
 		<div class="col-sm-10 col-md-10">
 			
 			<div id="myEditModal" class="modalEdit">

				<!-- Modal content -->
				<div class="modal-content">

					<form id="formId" method="post" action="<?php echo base_url(); ?>index.php/Main_controller/update">

 			 					<?php
									extract($records);
									//var_dump($records);
								?>

								<div class="form-group">
						      		<label for="id">ID:</label>
									<input type="number" class="form-control" name="id" id="id" value="<?php echo $id; ?>">
														      		 
						      	</div>


						      	<div class="form-group">
						      		<label for="datepicker">Date:</label>
									<input type="text" class="form-control" name="datepicker" id="datepicker" value="<?php echo $date; ?>">
														      		 
						      	</div>

						      	<div class="form-group">
						      		<label for="description">Description:</label>
						      		<input type="text" class="form-control" name="description" id="description" placeholder="Enter description" value="<?php echo $description; ?>">
						      	</div>
						      	<div class="form-group">
						      		<label for="amount">Amount:</label>
						      		<input type="number" class="form-control" name="amount" id="amount" value="<?php echo $amount; ?>" placeholder="Enter amount">
						      	</div>
						      	<div class="form-group">
						      		<label for="category">Category:</label>
						      		<select id="category" name="category" value="<?php echo $category; ?>" class="form-control">
						      			<option>select</option>
						      			<option>Fuel expenses</option>
						      			<option>Food expenses</option>
						      		</select>
						      	</div>
						      	<input type="submit" name="Submit" value="Save">
					</form> <!-- Expense insert form end -->

				</div>
				
			</div> <!-- model end -->



 			 		 		 			     
 		</div>
 	</div>
 </div>