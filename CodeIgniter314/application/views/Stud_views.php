<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Student data</title>
	<script type="text/javascript">
		function show_confirm(act,gotoid){
			if (act=="edit") {
				var r=confirm("Do you really want to edit? ");
			}

			else{
				var r=confirm("Do you really want to delete ?");
			}

			if(r==true){
				window.location="<?php echo base_url(); ?>index.php/Stud_controller/"+act+"/"+gotoid;
			}

		}
	</script>
</head>
<body>
	<h2>Simple CI CRUD operation</h2>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Address</th>
			<th>College name</th>
			<th colspan="2">Action</th>
			
		</tr>

		<?php foreach ($Studdata as $data) {?>
		<tr>
			<td><?php echo $data->id; ?></td>
			<td><?php echo $data->name; ?></td>
			<td><?php echo $data->address; ?></td>
			<td><?php echo $data->colz_name; ?></td>
			<td><a href="#" onClick="show_confirm('edit',<?php echo $data->id;?>)">Edit</a></td>
			<td><a href="#" onClick="show_confirm('delete',<?php echo $data->id;?>)">Delete</a></td>
		</tr>
		<?php } ?>
	</table>
	<p><a href="<?php echo base_url(); ?>index.php/Stud_controller/add_form">Insert new data</a></p>
</body>
</html>