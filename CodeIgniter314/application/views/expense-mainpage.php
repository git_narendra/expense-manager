<?php include "expense-header.php"; ?>

<div class="container-fluid">
	<nav class="navbar navbar-inverse">
		<!-- <div class="container-fluid"> -->
			<div class="navbar-header">
				<a href="#" class="navbar-brand"><span class="glyphicon glyphicon-cog"></span>Web Project Builder</a>
			</div>
			<div class="logout">
				<span class="glyphicon glyphicon-wrench"></span>
				<a href="<?php echo site_url('/Main_controller/logoutUser'); ?>">Logout</a>
			</div>
		<!-- </div> -->
	</nav>
	<div class="row">
		<div class="col-sm-2 col-md-2" id="tabClmn">
		
			<ul class="nav nav-tabs nav-stacked">
				<li><a data-toggle="tab" href="#dashboard"><span class="glyphicon glyphicon-dashboard"> Dashboard</a></li>
				<li><a data-toggle="tab" href="#myAccountContent"><span class="glyphicon glyphicon-user"> My account</a></li>
				<li><a data-toggle="tab" href="#expensesContent"><pan class="glyphicon glyphicon-credit-card"> Expenses</a></li>
				<li><a data-toggle="tab" href="#incomeContent"><span class="glyphicon glyphicon-usd" id="incomeTab"> Income</a></li>
				<li><a data-toggle="tab" href="#expensesCategoryContent"><span class="glyphicon glyphicon-align-left"> Expense category</a></li>
				<li><a data-toggle="tab" href="#incomeCategoryContent"><span class="glyphicon glyphicon-align-right"> Income category</a></li>
				<li><a data-toggle="tab" href="#aboutUsContent"><span class="glyphicon glyphicon-info-sign"> About us</a></li>

			</ul>
		</div> <!-- end of tab -->

		<!-- tab-content start -->
		<div class="col-sm-10 col-md-10">
			<div class="tab-content">

				<!-- start of dashboard -->
				<div id="dashboard" class="tab-pane fade in active">
		    	
		    		<div class="row">
		    			<!-- dashboard header -->
		    			<div class="dashboard"><h3>DASHBOARD</h3></div>
		    			<div class="col-sm-4 col-md-4 ">
		    				<div class="info-box">
		    					<div class="star-box">
		    						<span class="glyphicon glyphicon-star-empty"></span>
		    					</div>
		    					<div class="box-content">
		    						<p>TOTAL INCOME</p>
		    						<p id="totalIncome"></p>
		    					</div>
		    					
		    				</div>
		    			
		    			</div>
		    			<div class="col-sm-4 col-md-4">
		    				<div class="expenses-info-box">
		    					<div class="fire-box">
		    						<span class="glyphicon glyphicon-fire"></span>
		    					</div>
		    					<div class="expenses-box-content">
		    						<p>TOTAL EXPENCES</p>
		    						<p><?php 
		    							$sum=0;
			    						foreach ($records as $data) {
			    							$sum=$sum+$data->amount;
			    						} 
			    						echo $sum;
		    						?></p>
		    					</div>
		    					
		    				</div>
		    			</div>
		    			<div class="col-sm-4 col-md-4">
		    				<div class="today-info-box">
		    					<div class="bookmark-box">
		    						<span class="glyphicon glyphicon-bookmark"></span>
		    					</div>
		    					<div class="today-box-content">
		    						<p>EXPENCES TODAYS</p>
		    						<p>50000.12</p>
		    					</div>
		    					
		    				</div>
		    			</div>
		    		</div>
		    		<!-- end of dashboard header row-->

		    		<div class="row">
		    			<!-- div for last expenses -->
		    			<div class="col-sm-6 col-md-6">
				    		
				    		<div class="last-expense-content" data-spy="scroll" data-target=".navbar" data-offset="50">
				    			<h4>Last Expenses</h4><hr>
				    			<table class="table">
									<thead>
										<tr>
											<th>Date</th>
											<th>Description</th>
											<th>Amount</th>
											<th>Category</th>
											
										</tr>
									</thead>
									<tbody>
										<?php foreach ($records as $d) {?>
										<tr>
											<td><?php echo $d->date; ?></td>
											<td><?php echo $d->description; ?></td>
											<td><?php echo $d->amount; ?></td>
											<td><?php echo $d->category; ?></td>
											
										</tr>
									<?php }?>
		 
									</tbody>
									
								</table>
				    		</div>
				    		
				    	</div><!-- end of last expenses div -->

				    	<!-- div for monthly expenses -->
				    	<div class="col-sm-6 col-md-6">
				    		
				    		<div class="monthly-expense-content">
				    			<h4>Monthly Expenses</h4><hr>
				    			 <canvas id="monthlyExpensesChart">
								</canvas> 

				    		</div>
				    	</div> <!-- end of monthly expenses -->

		    		</div> <!-- end of second row of dashboard -->

		    	</div> <!-- end of dashboard -->

		    	<!-- script for monthlyExpensesChart -->
		    	<script type="text/javascript">
		    		
					// $(function () {
					    // Get the context of the canvas element we want to select
					    var ctx = document.getElementById("monthlyExpensesChart").getContext('2d');
						var myBarChart = new Chart(ctx).Bar(data, option);
						ctx.moveTo(0,0);
						ctx.lineTo(200,100);
						ctx.stroke();    
					// });

					// $(function () {
					    var option = {
					    responsive: true
					    };
					    // Get the context of the canvas element we want to select
					    // var ctx = document.getElementById("monthlyExpensesChart").getContext('2d');
					    // var myBarChart = new Chart(ctx).Bar(data, option);
					// });


					// $(function () {
					    
						var data = {
						    labels: ["January", "February", "March", "April", "May", "June", "July"],
						    datasets: [
						        {
						            label: "My First dataset",
						            fillColor: "rgba(220,220,220,0.5)",
						            strokeColor: "rgba(220,220,220,0.8)",
						            highlightFill: "rgba(220,220,220,0.75)",
						            highlightStroke: "rgba(220,220,220,1)",
						            data: [65, 59, 80, 81, 56, 55, 40]
						        },
						        {
						            label: "My Second dataset",
						            fillColor: "rgba(151,187,205,0.5)",
						            strokeColor: "rgba(151,187,205,0.8)",
						            highlightFill: "rgba(151,187,205,0.75)",
						            highlightStroke: "rgba(151,187,205,1)",
						            data: [28, 48, 40, 19, 86, 27, 90]
						        }
						    ]
						};

					    
						// });



		    	</script>
		    	<!-- end of monthlyExpensesChart -->
				
				<!-- start of myAccountContent -->
				<div id="myAccountContent" class="tab-pane fade">
					<div class="account-header">
						<h3>My account</h3>
					</div>
			      	<div class="account-content">
			      		<div class="col-md-4">
						  	<img src="<?php echo base_url(); ?>assets/images/demo_pic.png" class="img-rounded" alt="Cinque Terre" />
			      		</div>
			      		<div class="col-sm-8">
			      			<h3>Personal Information</h3>
			      			<label>Name:</label>
			      			<div class="panel panel-default">
			      				<div class="panel-body" id="usernameBox"></div>
			      			</div>
			      			<label>Address:</label>
			      			<div class="panel panel-default">
			      				<div class="panel-body" id="addressBox">balkumari,lalitpur</div>
			      			</div>
			      			<label>Email:</label>
			      			<div class="panel panel-default">
			      				<div class="panel-body" id="emailBox">chynarendra30@gmail.com</div>
			      			</div>

			      			<h3>Change password</h3>
			      			<form>
			      				<div class="form-group">
			      					<label for="cpass">Current password:</label>
			      					<input type="password" name="cpass" id="cpass" class="form-control">
			      				</div>
			      				<div class="form-group">
			      					<label for="address">New password:</label>
			      					<input type="password" name="npass" id="npass" class="form-control">
			      				</div>
			      				<div class="form-group">
			      					<label for="name">Confirm new password:</label>
			      					<input type="password" name="cnpass" id="cnpass" class="form-control">
			      				</div>

			      				<div class="form-group">
			      					<button type="button" id="btn btn-info">save</button>
			      				</div>
			      			</form>
			      		</div>		      		
			      	</div>

			      	<script type="text/javascript">
			      		$(document).ready(function(){

			      			$.ajax({
			      				url:"<?php echo site_url('/Main_controller/getUserInfo'); ?>",
			      				dataType:'json',
			      				type:'post',
			      				crossOrigin:true,
			      				success:function(data){
			      					console.log("data for my account=",data);
			      					//('#usernameBox').append(data[0]);
			      				},
			      				error:function(e){
			      					console.log("my account error",e);
			      				}
			      			});

			      		});
			      	</script>
			      
			    </div> <!-- end of myAccountContent -->

			    <!-- expensesContent-->
				<div id="expensesContent" class="tab-pane fade">
				    
				    <div class="row">
				    	<div class="col-md-12">
		    				<div class="expenses-header">
					    		<h4>Expenses</h4>
					      		<button type="button" class="btn btn-primary" id="btnAddExpenses"><span class="glyphicon glyphicon-plus">Add Expenses</button>
		    				</div> 
	    				</div><!-- end of col-md-12 -->
				    </div> <!-- end of row -->
	    			

	    			<script type="text/javascript">
		        		$(document).ready(function(){
		        			$("#btnAddExpenses").click(function(){
		        				$("#myExpenseModal").modal();
		        			});
		        		});
			        </script>

	    			<!-- The Modal for insert expenses data-->
					<div id="myExpenseModal" class="modal" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content -->
							<div class="modal-content">
							    <div class="modal-header">
							      <span class="close" data-dismiss="modal">&times;</span>
							      <h2>Expenses</h2>
							    </div>

							    <div class="modal-body">

							    <!-- Expenses insert form -->
							      <form id="formId" method="post" action="<?php echo base_url(); ?>index.php/Main_controller/insertExpensesData">
							      	<div class="form-group">
							      		<label for="date">Date:</label>
										<input type="text" class="form-control" name="dateE" id="date">
															      		 
							      	</div>

							      	<div class="form-group">
							      		<label for="description">Description:</label>
							      		<input type="text" class="form-control" name="descriptionE" id="description" placeholder="Enter description">
							      	</div>
							      	<div class="form-group">
							      		<label for="amount">Amount:</label>
							      		<input type="number" class="form-control" name="amountE" id="amount" placeholder="Enter amount">
							      	</div>
							      	<div class="form-group">
							      		<label for="ecategory">Category:</label>
							      		<select id="ecategory" name="categoryE" class="form-control">
							      			<option>select</option>
							      			<option>Fuel expenses</option>
							      			<option>Food expenses</option>
							      		</select>
							      	</div>
							      	<input type="submit" name="Submit" value="Save">
							      </form> <!-- Expense insert form end -->

							    </div>

							</div>	<!-- end of modal-content -->		

						</div><!-- end of modal dialog -->
							
					</div><!-- end of Model for insert expenses data -->

					<!-- script for datepicker -->
					<script type="text/javascript">
			           	$(document).ready(function() {
							$("#date").datepicker();
											   
							});
		        	</script>

			        <div class="row">
			        	<div class="col-md-12">
		    				<!-- show expenses table data -->
						    <div class="expense-data-content">
					    		<h4>Expenses Data</h4>
								<table class="table table-hover">
									<thead>
										<tr>
											<th>ID</th>
											<th>Date</th>
											<th>Description</th>
											<th>Amount</th>
											<th>Category</th>
											<th colspan="2">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($records as $d) {?>
										<tr>
											<td><?php echo $d->id; ?></td>
											<td><?php echo $d->date; ?></td>
											<td><?php echo $d->description; ?></td>
											<td><?php echo $d->amount; ?></td>
											<td><?php echo $d->category; ?></td>
											
											<td><a href="#" onClick="show_confirm('edit',<?php echo $d->id;?>)" data-toggle="modal" data-target="#myEditModal"><span class="glyphicon glyphicon-pencil"></span></a></td>
											<td><a href="<?php echo site_url('Main_controller/delete/'.$d->id); ?>" onClick="return confirm('Do you really want to delete ?'); "><span class="glyphicon glyphicon-trash"></span></a></td>
										</tr>
									<?php }?>
		 
									</tbody>
									
								</table>
						    </div> <!-- end of expense-data-content -->
			    		</div><!-- end of col-md-12 -->
			        </div> <!-- end of row -->
	    			
				</div> <!-- end expensesContent --> 	

				<!-- The Modal for edit expenses data-->
				<div id="myEditModal" class="modal" role="dialog">

					<div class="modal-dialog">
						<!-- Modal content -->
						<div class="modal-content">

						    <div class="modal-header">
						      <span class="close" data-dismiss="modal">&times;</span>
						      <h2>Edit Expense</h2>
						    </div> <!-- end of modal-header -->

						    <div class="modal-body">

						    	<!-- Expenses edit form -->
						      <form id="formEdit" method="post" action="<?php echo base_url(); ?>index.php/Main_controller/update">

						      	<div class="form-group">
						      		<label for="id">ID:</label>
									<input type="text" class="form-control" name="id" id="id">
														      		 
						      	</div>

						      	<div class="form-group">
						      		<label for="date">Date:</label>
									<input type="text" class="form-control" name="date" id="date">
														      		 
						      	</div>

						      	<div class="form-group">
						      		<label for="description">Description:</label>
						      		<input type="text" class="form-control" name="description" id="description" placeholder="Enter description">
						      	</div>
						      	<div class="form-group">
						      		<label for="amount">Amount:</label>
						      		<input type="number" class="form-control" name="amount" id="amount" placeholder="Enter amount">
						      	</div>
						      	<div class="form-group">
						      		<label for="category">Category:</label>
						      		<select id="eeditcategory" name="categoryE" class="form-control">
						      			<option>select</option>
						      			<option>Fuel expenses</option>
						      			<option>Food expenses</option>
						      		</select>
						      	</div>
						      	<input type="submit" name="Submit" value="Save">
						      </form> <!-- Expense edit form end -->

						      <!-- script for datepicker -->
						       <script type="text/javascript">
	           						$(document).ready(function() {
									    $("#datepicker").datepicker();
									    
									});
	    						</script>
						    </div> <!-- end of modal-body -->
						</div> <!-- end of modal-content -->
					</div> <!-- end of modal-dialog -->

				</div> <!-- end of Modal for edit expenses data -->
				
		    	<!-- script for Modal for edit expenses data -->
				<script type="text/javascript">
					
					$.ajax({

				        url:"<?php echo site_url('/Main_controller/showAllExpenseCategory')?>",
				        type:'POST',
				        dataType: 'json',
				       	crossOrigin: true,
				        success: function(data){
				        	console.log("expense category data=",data);
				        	var row='';
				        	$.each(data.record,function(key,value){
				        		row+="<option>"+value.category_name+"</option>";
				        	});
					          $('#ecategory').append(row);
					          $('#eeditcategory').append(row);
					        },

					         error:function(e){
					         	console.log(e);
					         	
					         }
					
				    });

					function show_confirm(act,gotoid){
					
						console.log(gotoid);
						console.log(act);

						$.ajax({

					        url:"<?php echo site_url('/Main_controller/edit/')?>"+gotoid,
					        type:'POST',
					        dataType: 'json',
					       	crossOrigin: true,
					        success: function(data){
					        	console.log("data=",data);
					        	var result=JSON.stringify(data);
					        	var arrayData=JSON.parse(result);
					        	console.log("id=",arrayData.data.id);

					        	$('[name="id"]').val(arrayData.data.id);
					        	$('[name="date"]').val(arrayData.data.date);
					        	$('[name="description"]').val(arrayData.data.description);
					        	$('[name="amount"]').val(arrayData.data.amount);
					        	$('[name="category"]').val(arrayData.data.category);
					        	
					        	 //modal.style.display = "block";
					        	
					            
					        },

					         error:function(e){
					         	console.log(e);
					         	
					         }
					
				    	});
			    	
					
					}

					

				</script> <!-- end of script of Modal for edit expenses data-->

  
			    <!-- income content start -->
			   	<div class="tab-pane fade" id="incomeContent">

			    	<script type="text/javascript">
				    	$(document).ready(function(){
				    		
				    			$.ajax({
						        url:"<?php echo site_url('/Main_controller/showAllIncomeData')?>",
						        type:'POST',
						        dataType: 'json',
						        crossOrigin: true,
						        success: function(data){
						        	var row='';
						        	var sum=[];
						        	var totalIncomeAmount=0;
						        	$.each(data.records, function(index, element) {
									   var gotoid=element.amount;
									   //console.log("id=",gotoid);
								    	row+="<tr><td>"+element.id+"</td><td>"+element.date+"</td><td>"+element.description+"</td><td>"+element.amount+"</td><td>"+element.category+"</td><td>"+"<a href='#' onclick='edit_income("+element.id+")' data-toggle='modal' data-target='#myIncomeEditModal'><span class='glyphicon glyphicon-pencil'></span></a>"+"</td><td>"+"<a href='#' onclick='delete_income("+element.id+")'><span class='glyphicon glyphicon-trash'></span></a>"+"</td></tr>";
								    	sum.push(gotoid);

									});

									for(var i=0;i<sum.length;i++) {
									    
									    var sumElement=sum[i];
									    var numAmount=parseInt(sumElement);
									    console.log("numAmount=",numAmount);
									    totalIncomeAmount+=numAmount;
									    
									};
									console.log("sum total",totalIncomeAmount);
									$('#incomeTable').append(row);
									$('#totalIncome').append(totalIncomeAmount);
								},
						         error:function(e){
						         	console.log("error!",e);
						         	
						         }
							
					    		});


					    		$.ajax({

							        url:"<?php echo site_url('/Main_controller/showAllIncomeCategory')?>",
							        type:'POST',
							        dataType: 'json',
							       	crossOrigin: true,
							        success: function(data){
							        	console.log("income category data=",data);
							        	var row='';
							        	$.each(data.record,function(key,value){
							        		row+="<option>"+value.category_name+"</option>";
							        	})
								          $('#icategory').append(row);
								          $('#ieditcategory').append(row);
								        },

								         error:function(e){
								         	console.log(e);
								         	
								         }
					
				    			});

						});

						function add_income(){

							$.ajax({
									url:"<?php echo site_url('/Main_controller/insertIncomeData'); ?>",
									type:"POST",
									data:$('#IncomeInsertform').serialize(),
									dataType:"JSON",
									
									success:function(data){
										//alert("income data updated");
										console.log("inside add_income");
										location.reload();
									},
									error:function(e){
										console.log(e);
									}
								});

						}

						function update_income(){

								$.ajax({
									url:"<?php echo site_url('/Main_controller/updateIncomeData'); ?>",
									type:"POST",
									data:$('#incomeEditForm').serialize(),
									dataType:"JSON",
									
									success:function(data){
										//alert("income data updated");
										console.log("inside update_income");
										location.reload();
									},
									error:function(e){
										console.log(e);
									}
								});
						}

						function delete_income(element){
							console.log("inside delete_income function");
			    			console.log("id=",element);
			    			if(confirm("Are you sure want to delete?")){
			    				$.ajax({
			    					url:"<?php echo site_url('/Main_controller/deleteIncome'); ?>/"+element,
			    					type:"POST",
			    					dataType:"JSON",
			    					crossOrigin: true,
			    					success:function(data){
			    						console.log("delete success function");
			    						console.log(data);
			    						location.reload();

			    					},
			    					error:function(e){
			    						console.log("error!",e);
			    					}
			    				});
			    			}

			    			return false;
			    		}

			    		function edit_income(element){
							console.log("inside edit_income function");
			    			console.log("id=",element);
			    			
		    				$.ajax({
		    					url:"<?php echo site_url('/Main_controller/editIncome'); ?>/"+element,
		    					type:"POST",
		    					dataType:"JSON",
		    					crossOrigin: true,
		    					success:function(data){
		    						console.log("edit_income success function");
		    						console.log(data);
									var result=JSON.stringify(data);
					        		var arrayData=JSON.parse(result);
		    						//console.log("income id:",arrayData.data.id);
		    						$('[name="idIncome"]').val(arrayData.data.id);
						        	$('[name="editIncomeDate"]').val(arrayData.data.date);
						        	$('[name="descriptionIncome"]').val(arrayData.data.description);
						        	$('[name="amountIncome"]').val(arrayData.data.amount);
						        	$('[name="categoryIncome"]').val(arrayData.data.category);

		    					},
		    					error:function(e){
		    						console.log("error!",e);
		    					}
		    				});
			    			
			    			return false;
			    		}
				    		
					</script>

					<div class="row">
						<div class="col-md-12">
		    				<div class="income-header">
					    		<h4>Income</h4>
					      		<button type="button" class="btn btn-primary" id="btnAddIncome" data-toggle="modal" data-target="#myIncomeModal"><span class="glyphicon glyphicon-plus">Add Income</button>
		    				</div> <!-- end of income-header -->
	    				</div><!-- end of col-md-12 -->
					</div> <!-- end of row -->
			   		

	    				<!-- starts myIncomeModal-->
						<div id="myIncomeModal" class="modal" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content -->
								<div class="modal-content">
								    <div class="modal-header">
								      <span class="close" data-dismiss="modal">&times;</span>
								      <h2>Income Add</h2>
								    </div>

								    <div class="modal-body">

									  <!-- Expenses insert form -->
								      <form id="IncomeInsertform" method="post" action="#">
								      	<div class="form-group">
								      		<label for="incomeDate">Date:</label>
											<input type="text" class="form-control" name="dateI" id="incomeDate">      		 
								      	</div>

								      	<div class="form-group">
								      		<label for="description">Description:</label>
								      		<input type="text" class="form-control" name="descriptionI" id="description" placeholder="Enter description">
								      	</div>
								      	<div class="form-group">
								      		<label for="amount">Amount:</label>
								      		<input type="number" class="form-control" name="amountI" id="amount" placeholder="Enter amount">
								      	</div>
								      	<div class="form-group">
								      		<label for="category">Category:</label>
								      		<select id="icategory" name="categoryI" class="form-control">
								      			<option>select</option>
								      			
								      		</select>
								      	</div>
								      	<input type="submit" onclick="add_income()" name="Submit" value="Save">
								      </form> <!-- Income insert form end -->
  
								    </div> <!-- end of modal-body -->

								</div>	<!-- end of modal-content -->		

							</div><!-- end of modal dialog -->
							
						</div><!-- end of myIncomeModal-->

						<!-- script for datepicker -->
						<script type="text/javascript">
				           	$(document).ready(function() {
								$("#incomeDate").datepicker();
												   
							});
			        	</script>

						
			        <div class="row">
			        	<div class="col-md-12">
		    				<!-- show income table data -->
						    <div class="expense-data-content">
					    		<h4>Income Data</h4>
								<table class="table table-hover" id="incomeTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>Date</th>
											<th>Description</th>
											<th>Amount</th>
											<th>Category</th>
											<th colspan="2">Action</th>
										</tr>
									</thead>
									<tbody>
									
									</tbody>
									
								</table>
						    </div> <!-- end of expense-data-content -->
			    		</div><!-- end of col-md-12 -->
			        </div> <!-- end of row -->
	    			

			    	<!-- starts myIncomeEditModal-->
					<div id="myIncomeEditModal" class="modal" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content -->
							<div class="modal-content">
							    <div class="modal-header">
							      <span class="close" data-dismiss="modal">&times;</span>
							      <h2>Income Edit</h2>
							    </div>

							    <div class="modal-body">

								  <!-- Expenses edit form -->
							      <form id="incomeEditForm" method="post" action="#">

							      	<div class="form-group">
							      		<label for="id">ID:</label>
										<input type="text" class="form-control" name="idIncome" id="id">      		 
							      	</div>
							      	<div class="form-group">
							      		<label for="editIncomeDate">Date:</label>
										<input type="text" class="form-control" name="editIncomeDate" id="editIncomeDate">      		 
							      	</div>

							      	<div class="form-group">
							      		<label for="description">Description:</label>
							      		<input type="text" class="form-control" name="descriptionIncome" id="description" placeholder="Enter description">
							      	</div>
							      	<div class="form-group">
							      		<label for="amount">Amount:</label>
							      		<input type="number" class="form-control" name="amountIncome" id="amount" placeholder="Enter amount">
							      	</div>
							      	<div class="form-group">
							      		<label for="category">Category:</label>
							      		<select id="ieditcategory" name="categoryIncome" class="form-control">
							      			<option class="active">select</option>
							      		</select>
							      	</div>
							      	<input type="submit" onclick="update_income()" name="Submit" value="update">
							      </form> <!-- Income edit form end -->

							    </div> <!-- end of modal-body -->

							</div>	<!-- end of modal-content -->		

						</div><!-- end of modal dialog -->
						
					</div><!-- end of myIncomeEditModal-->

					<!-- script for datepicker -->
					<script type="text/javascript">
			           	$(document).ready(function() {
							$("#editIncomeDate").datepicker();
											   
						});
		        	</script>

			   	</div>
			    <!-- end of income content -->

			    <!-- start of expensesCategoryContent -->
			    <div class="tab-pane fade" id="expensesCategoryContent">
			    	
			    	<script type="text/javascript">
				    	$(document).ready(function(){
				    		console.log("category script");
			    			$.ajax({
					        url:"<?php echo site_url('/Main_controller/showAllExpenseCategory')?>",
					        type:'POST',
					        dataType: 'json',
					        crossOrigin: true,
					        success: function(data){
					        	console.log("category response:",data);
					        	var row='';
					        	$.each(data.record, function(index, element) {
								   var gotoid=element.id;
								   console.log("category id=",gotoid);
							    	row+="<tr><td>"+element.id+"</td><td>"+element.category_name+"</td><td>"+"<a href='#' onclick='edit_ecategory("+element.id+")' data-toggle='modal' data-target='#myExpenseCategoryEditModal'><span class='glyphicon glyphicon-pencil'></span></a>"+"</td><td>"+"<a href='#' onclick='delete_ecategory("+element.id+")'><span class='glyphicon glyphicon-trash'></span></a>"+"</td></tr>";
								});
								$('#ecategoryTable').append(row);
							},
					         error:function(e){
					         	console.log("category error!",e);
					         	
					         }
						
				    		});

						});

						function add_expensecategory(){

							$.ajax({
								url:"<?php echo site_url('/Main_controller/insertExpenseCategory'); ?>",
								type:"POST",
								data:$('#expenseCatgoryInsertform').serialize(),
								dataType:"JSON",

								success:function(data){
									//alert("expense category");
									console.log("inside add_expensecategory");
									//location.reload();
								},
								error:function(e){
									console.log("expenses category error!",e);
								}
							});

						}

						function update_ecategory(){
								console.log("inside update_ecategory");
								$.ajax({
									url:"<?php echo site_url('/Main_controller/updateExpenseCategory'); ?>",
									type:"POST",
									data:$('#ecategoryEditForm').serialize(),
									dataType:"JSON",
									// contentType:'application/json',
									success:function(data){
										//alert("income data updated");
										console.log("update function");
										location.reload();
									},
									error:function(e){
										console.log("category update error!",e);
									}
								});
						}

						function delete_ecategory(element){
							console.log("inside delete_ecategory function");
			    			console.log("id=",element);
			    			if(confirm("Are you sure want to delete?")){
			    				$.ajax({
			    					url:"<?php echo site_url('/Main_controller/deleteExpenseCategory'); ?>/"+element,
			    					type:"POST",
			    					dataType:"JSON",
			    					crossOrigin: true,
			    					success:function(data){
			    						console.log("delete success function");
			    						console.log(data);
			    						location.reload();

			    					},
			    					error:function(e){
			    						console.log("error!",e);
			    					}
			    				});
			    			}

			    			return false;
			    		}

			    		function edit_ecategory(element){
							
		    				$.ajax({
		    					url:"<?php echo site_url('/Main_controller/editExpenseCategory'); ?>/"+element,
		    					type:"POST",
		    					dataType:"json",
		    					crossOrigin: true,
		    					success:function(data){
		    						//console.log("category edit");
		    						console.log("edit expense category data:",data);
						      		$.each(data,function(key,d){
									    $("input[name='idEcategory']").val(d.id);
									    $("input[name='editEcategoryName']").val(d.category_name);
									});

		    					},
		    					error:function(e){
		    						console.log("edit category error!",e);
		    					}
		    				});
			    			
			    			return false;
			    		}
				    		
					</script>

					<div class="row">
						<div class="col-md-12">
		    				<div class="income-header">
					    		<h4>Expenses Category</h4>
					      		<button type="button" class="btn btn-primary" id="btnAddIncome" data-toggle="modal" data-target="#myExpenseCategoryModal"><span class="glyphicon glyphicon-plus">Add expenses category</button>
		    				</div> <!-- end of income-header -->
	    				</div><!-- end of col-md-12 -->
					</div> <!-- end of row -->
				   		

    				<!-- starts myExpenseCategoryModal-->
					<div id="myExpenseCategoryModal" class="modal" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content -->
							<div class="modal-content">
							    <div class="modal-header">
							      <span class="close" data-dismiss="modal">&times;</span>
							      <h2>Add expenses category</h2>
							    </div>

							    <div class="modal-body">

								  <!-- Expenses insert form -->
							      <form id="expenseCatgoryInsertform" method="post" action="#">
							      	<div class="form-group">
							      		<label for="expenseCategoryName">Category name:</label>
										<input type="text" class="form-control" name="expenseCategoryName" id="expenseCategoryName">      		 
							      	</div>

							      <input type="submit" onclick="add_expensecategory()" name="Submit" value="Save">
							      
							      </form> <!-- Income insert form end -->

							    </div> <!-- end of modal-body -->

							</div>	<!-- end of modal-content -->		

						</div><!-- end of modal dialog -->
						
					</div><!-- end of myExpenseCategoryModal-->

			        <div class="row">
			        	<div class="col-md-12">
		    				<!-- show income table data -->
						    <div class="expense-data-content">
					    		<h4>Expesnes category Data</h4>
								<table class="table table-hover" id="ecategoryTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>Category name</th>
											<th colspan="2">Action</th>
										</tr>
									</thead>
									<tbody>
									
									</tbody>
									
								</table>
						    </div> <!-- end of expense-data-content -->
			    		</div><!-- end of col-md-12 -->
			        </div> <!-- end of row -->
	    			

			    	<!-- starts myIncomeEditModal-->
					<div id="myExpenseCategoryEditModal" class="modal" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content -->
							<div class="modal-content">
							    <div class="modal-header">
							      <span class="close" data-dismiss="modal">&times;</span>
							      <h2>Expense category edit</h2>
							    </div>

							    <div class="modal-body">
								  <!-- Expenses insert form -->
							      <form id="ecategoryEditForm" method="post" action="#">

							      	<div class="form-group">
							      		<label for="id">ID:</label>
										<input type="text" class="form-control" name="idEcategory" id="id">      		 
							      	</div>
							      	<div class="form-group">
							      		<label for="editEcategoryName">Category name:</label>
										<input type="text" class="form-control" name="editEcategoryName" id="editEcategoryName">      		 
							      	</div>
							      	<input type="submit" onclick="update_ecategory()" name="Submit" value="update">
							      </form> <!-- Income insert form end -->

							    </div> <!-- end of modal-body -->

							</div>	<!-- end of modal-content -->		

						</div><!-- end of modal dialog -->
						
					</div><!-- end of myIncomeEditModal-->

					<!-- script for datepicker -->
					<script type="text/javascript">
			           	$(document).ready(function() {
							$("#editIncomeDate").datepicker();
											   
						});
		        	</script>

			   	<!-- </div> -->
			    </div>
			    <!-- end of expensesCategoryContent -->

			    <!-- start of incomeCategoryContent -->
			    <div class="tab-pane fade" id="incomeCategoryContent">
			    	
			    	<script type="text/javascript">
				    	$(document).ready(function(){
				    		console.log("income category script");
			    			$.ajax({
					        url:"<?php echo site_url('/Main_controller/showAllIncomeCategory')?>",
					        type:'POST',
					        dataType: 'json',
					        crossOrigin: true,
					        success: function(data){
					        	console.log("category response:",data);
					        	var row='';
					        	$.each(data.record, function(index, element) {
								   var gotoid=element.id;
								   console.log("category id=",gotoid);
							    	row+="<tr><td>"+element.id+"</td><td>"+element.category_name+"</td><td>"+"<a href='#' onclick='edit_icategory("+element.id+")' data-toggle='modal' data-target='#myIncomeCategoryEditModal'><span class='glyphicon glyphicon-pencil'></span></a>"+"</td><td>"+"<a href='#' onclick='delete_incomecategory("+element.id+")'><span class='glyphicon glyphicon-trash'></span></a>"+"</td></tr>";
								});
								$('#icategoryTable').append(row);
							},
					         error:function(e){
					         	console.log("category error!",e);
					         	
					         }
						
				    		});

						});

						function add_incomecategory(){

							$.ajax({
								url:"<?php echo site_url('/Main_controller/insertIncomeCategory'); ?>",
								type:"POST",
								data:$('#incomeCatgoryInsertform').serialize(),
								dataType:"JSON",

								success:function(data){
									alert("expense category");
									console.log("inside add_expesecategory");
									//location.reload();
								},
								error:function(e){
									console.log("expenses category error!",e);
								}
							});

						}

						function update_icategory(){
								console.log("update_icategory function called");
								$.ajax({
									url:"<?php echo site_url('/Main_controller/updateIncomeCategory'); ?>",
									type:"POST",
									data:$('#icategoryEditForm').serialize(),
									dataType:"JSON",
									// contentType:'application/json',
									success:function(data){
										//alert("income data updated");
										console.log("success function of update_icategory");
										//location.reload();
									},
									error:function(e){
										console.log("income cagtegory update error!",e);
									},
									complete:function(){
										console.log("complete function");
									}
								});
						}

						function delete_incomecategory(element){
							console.log("inside delete_incomecategory function");
			    			console.log("id=",element);
			    			if(confirm("Are you sure want to delete?")){
			    				$.ajax({
			    					url:"<?php echo site_url('/Main_controller/deleteIncomeCategory'); ?>/"+element,
			    					type:"POST",
			    					dataType:"JSON",
			    					crossOrigin: true,
			    					success:function(data){
			    						console.log("delete success function");
			    						console.log(data);
			    						location.reload();

			    					},
			    					error:function(e){
			    						console.log("error!",e);
			    					}
			    				});
			    			}

			    			return false;
			    		}

			    		function edit_icategory(element){
							
		    				$.ajax({
		    					url:"<?php echo site_url('/Main_controller/editIncomeCategory'); ?>/"+element,
		    					type:"POST",
		    					dataType:"json",
		    					crossOrigin: true,
		    					success:function(data){
		    						console.log("income category edit");

						      		$.each(data,function(key,d){
									    $("input[name='idIcategory']").val(d.id);
									    $("input[name='editIcategoryName']").val(d.category_name);
									});

		    					},
		    					error:function(e){
		    						console.log("edit income category error!",e);
		    					}
		    				});
			    			
			    			return false;
			    		}
				    		
					</script>

					<div class="row">
						<div class="col-md-12">
		    				<div class="income-header">
					    		<h4>Income Category</h4>
					      		<button type="button" class="btn btn-primary" id="btnAddIncome" data-toggle="modal" data-target="#myIncomeCategoryModal"><span class="glyphicon glyphicon-plus">Add income category</button>
		    				</div> <!-- end of income-header -->
	    				</div><!-- end of col-md-12 -->
					</div> <!-- end of row -->
				   		

    				<!-- starts myExpenseCategoryModal-->
					<div id="myIncomeCategoryModal" class="modal" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content -->
							<div class="modal-content">
							    <div class="modal-header">
							      <span class="close" data-dismiss="modal">&times;</span>
							      <h2>Add income category</h2>
							    </div>

							    <div class="modal-body">

								  <!-- Income insert form -->
							      <form id="incomeCatgoryInsertform" method="post" action="#">
							      	<div class="form-group">
							      		<label for="incomeCategoryName">Category name:</label>
										<input type="text" class="form-control" name="incomeCategoryName" id="incomeCategoryName">      		 
							      	</div>

							      <input type="submit" onclick="add_incomecategory()" name="Submit" value="Save">
							      
							      </form> <!-- Income insert form end -->

							    </div> <!-- end of modal-body -->

							</div>	<!-- end of modal-content -->		

						</div><!-- end of modal dialog -->
						
					</div><!-- end of myExpenseCategoryModal-->

			        <div class="row">
			        	<div class="col-md-12">
		    				<!-- show income table data -->
						    <div class="expense-data-content">
					    		<h4>Income category Data</h4>
								<table class="table table-hover" id="icategoryTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>Category name</th>
											<th colspan="2">Action</th>
										</tr>
									</thead>
									<tbody>
									
									</tbody>
									
								</table>
						    </div> <!-- end of expense-data-content -->
			    		</div><!-- end of col-md-12 -->
			        </div> <!-- end of row -->
	    			

			    	<!-- starts myIncomeEditModal-->
					<div id="myIncomeCategoryEditModal" class="modal" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content -->
							<div class="modal-content">
							    <div class="modal-header">
							      <span class="close" data-dismiss="modal">&times;</span>
							      <h2>Income category edit</h2>
							    </div>

							    <div class="modal-body">
								  <!-- Expenses insert form -->
							      <form id="icategoryEditForm" method="post" action="#">

							      	<div class="form-group">
							      		<label for="id">ID:</label>
										<input type="text" class="form-control" name="idIcategory" id="id">      		 
							      	</div>
							      	<div class="form-group">
							      		<label for="editIcategoryName">Category name:</label>
										<input type="text" class="form-control" name="editIcategoryName" id="editIcategoryName">      		 
							      	</div>
							      	<input type="submit" onclick="update_icategory()" name="Submit" value="update">
							      </form> <!-- Income insert form end -->

							    </div> <!-- end of modal-body -->

							</div>	<!-- end of modal-content -->		

						</div><!-- end of modal dialog -->
						
					</div><!-- end of myIncomeEditModal-->

					<!-- script for datepicker -->
					<script type="text/javascript">
			           	$(document).ready(function() {
							$("#editIncomeDate").datepicker();
											   
						});
		        	</script>

			    </div>
			    <!-- end of incomeCategoryContent -->

			    <!-- start of users content -->
			    <div class="tab-pane fade" id="aboutUsContent">
				    <p>This project is build with <a href="http://www.webprojectbuilder.com">Web Project Builder</a>.</p>
	                <p><a href="http://www.webprojectbuilder.com">Web Project Builder</a> allows you to generate PHP code of projects using simple user interface without knowledge of programming. This includes CRUD Generator, User Authentication, User Roles, Permission and much more. </p>
			    </div>
			    <!-- end of users content -->

			</div> <!-- end of tab-content -->
		</div> <!-- end of col-sm-10 col-md-10 class -->
	</div> <!-- end of row -->
</div> <!-- end of container -->