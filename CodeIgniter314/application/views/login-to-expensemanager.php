<?php 
	include "expense-header.php";
 ?>

 <div class="container-fliud">
 	<div class="row">
 		<div class="loginDiv">
 			<h3>User login</h3><hr>
		 	<form class="form-horizontal" id="loginForm" action="<?php echo site_url('/Main_controller/login_to_expensemanager'); ?>" method="post">
		 		<div class="form-group">
		 			<label class="control-label col-sm-2" for="email">Email:</label>
		 			<div class="col-sm-10">
		 				<input type="text" name="email" id="email" class="form-control">
		 			</div>
		 			
		 		</div>

		 		<div class="form-group">
		 			<label class="control-label col-sm-2" for="pass">Password:</label>
		 			<div class="col-sm-10">
		 				<input type="password" name="pass" id="pass" class="form-control">
		 			</div>
		 			
		 		</div>
		 		<button type="submit" class="btn btn-primary" id="subBtn">login</button>
		 		<button type="button" class="btn btn-primary" id="regBtn" data-toggle="modal" data-target="#myRegisterModal">Register</button>
		 	</form>

		 	<!-- The myRegisterModal-->
			<div id="myRegisterModal" class="modal" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content -->
					<div class="modal-content">
					    <div class="modal-header">
					      <span class="close" data-dismiss="modal">&times;</span>
					      <h2>Register</h2>
					    </div>

					    <div class="modal-body">

						    <!-- Register form -->
						    <form id="registerForm" method="post" action="#">
						      	<div id="errormsg" class="form-group"></div>
						      	<div class="form-group">
						      		<label for="name">Name:</label>
									<input type="text" class="form-control" name="name" id="name" placeholder="enter your full name">				      		 
						      	</div>

						      	<div class="form-group">
						      		<label for="address">Address:</label>
						      		<input type="text" class="form-control" name="address" id="address" placeholder="enter your address">
						      	</div>

						      	<div class="form-group">
						      		<label for="email">Email:</label>
						      		<input type="email" class="form-control" name="email" id="email" placeholder="enter your email">
						      	</div>

						      	<div class="form-group">
						      		<label for="rpass">Password:</label>
						      		<input type="password" class="form-control" name="pass" id="rpass" placeholder="password">
						      	</div>

						      	<div class="form-group">
						      		<label for="cpass">Confirm password:</label>
						      		<input type="password" class="form-control" name="cpass" id="cpass" placeholder="password">
						      	</div>
						      	
						      	<!-- <input type="submit" name="submit" onclick="submitTest()" value="Register"> -->
						      	<button id="sub-btn">Register</button>
						    </form> <!-- Register end -->

					    </div>

					</div>	<!-- end of modal-content -->		

				</div><!-- end of modal dialog -->

				<script type="text/javascript">
					
					//var cpass=$('#cpass');
					
					$(document).ready(function(){
						console.log("register script");

						$('#subBtn').on('click',function(e){
							$.ajax({
								url:"<?php echo site_url('/Main_controller/login_to_expensemanager'); ?>",
								type:'post',
								dataType:'json',
								data:$('#loginForm').serialize(),
								crossOrigin:true,
								success:function(data){
									console.log("data login process success");
								},
								error:function(e){
									console.log('login error',e);
								}

							});
						});

						$("#sub-btn").on("click",function(e){

							 e.preventDefault();
							 console.log("submit button clicked");
							 var pass=document.getElementById('rpass').value;
							 var confirmpass=document.getElementById('cpass').value;
							 
							 if (pass!=confirmpass) {
							 	
							 	$("#errormsg").append("<p>your password is not matched !</p>");
							 	console.log("inside if");
							 	//location.reload();

							 }
							 else{
							 	console.log("inside else");
							 	$.ajax({
									url:"<?php echo site_url('/Main_controller/insertRegisteredData'); ?>",
									type:'POST',
									data:$('#registerForm').serialize(),
									dataType:'JSON',
									crossOrigin:true,
									success:function(data){
										console.log('registered data response=',data);
										location.reload();
									},
									error:function(e){
										console.log('registered data error!',e);
									}

								});
							 }


						});

					});

				</script>
					
			</div><!-- end of myRegisterModal -->
			<div id="errormsg"></div>
 		</div> <!-- end of loginDiv -->
 		<!-- <div class="loginError"><?php echo $msg;?></div> -->
 	</div> <!-- end of row -->
 </div><!-- end of container-fliud -->