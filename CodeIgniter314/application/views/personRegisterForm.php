<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form method="post" action="<?php echo site_url('formController/saveData') ;?>">
		<table>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name"></td>
			</tr>

			<tr>
				<td>Address:</td>
				<td><input type="text" name="address"></td>
			</tr>

			<tr>
				<td>Gender:</td>
				<td><input type="radio" name="gender" value="male">Male</td>
				<td><input type="radio" name="gender" value="female">Female</td>
			</tr>

			<tr>
				<td><input type="submit" name="" value="Save"></td>
			</tr>
		</table>
	</form>

	<table border="1">
		<thead>
			<th>Id</th>
			<th>Name</th>
			<th>Address</th>
			<th>Gender</th>
		</thead>
		<tbody>
			<?php 
				$this->load->view('main_model');
				foreach ($this->main_model->fetchTable() as $row) {
					echo "<tr>
							<td>$row->id</td>
							<td>$row->name</td>
							<td>$row->address</td>
							<td>$row->gender</td>
						</tr>";
				}
			 ?>
		</tbody>
	</table>
</body>
</html>