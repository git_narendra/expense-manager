<?php include 'header.php' ?>

<div class="container">
	<div class="row">
		<div class="col-sm-4">
			<h3>Please Sign In</h3>

			<?php if (isset($error) && $error): ?>
	          <div class="alert alert-error">
	            <a class="close" data-dismiss="alert" href="#">×</a>Incorrect Username or Password!
	          </div>
        	<?php endif; ?>
        	
			<form method="post" action="<?php echo base_url(); ?>index.php/Login_controller/login_user">
				<table>
					<tr>
						<td><input type="text" name="uname" placeholder="user name"></td>
					</tr>
					<tr>
						<td><input type="password" name="pwd" placeholder="password"></td>
					</tr>
					<tr>
						<td><input type="submit" name="sub" value="Login"></td>
					</tr>
				</table>
			</form>
			<p><a href="<?php echo base_url(); ?>index.php/Login_controller/signupUser"><strong>Sign Up</strong></a></p>
		</div>
	</div>
	
</div>