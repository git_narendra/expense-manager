<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function show_conform(act,id){
			if(act=='edit'){
				var r=confirm("Do you really want to edit?");
			}

			else{
				var r=confirm("Do you really want to delete?");
			}

			if(r==true){
				window.location="<?php base_url(); ?>index.php/Main_controller/"+act+"/"+id+;
			}
		}
	</script>
</head>
<body>
	<h3>Expenses Data</h3>
	<table border="1">
		<tr>
			<th>Date</th>
			<th>Description</th>
			<th>Amount</th>
			<th>Category</th>
			<th colspan="2">Action</th>
		</tr>
		<?php foreach ($expenseData as $data) {?>
			<tr>
				<td><?php echo $data->date; ?></td>
				<td><?php echo $data->description; ?></td>
				<td><?php echo $data->amount; ?></td>
				<td><?php echo $data->category; ?></td>
				<td><a href="#" onclick="show_conform('edit',<?php echo $data->id; ?>)">edit</a></td>
				<td><a href="#" onclick="show_conform('delete',<?php echo $data->id; ?>)">delete</a></td>
			</tr>
		<?php }?>		
	</table>
</body>
</html>