<?php 

	/**
	* 
	*/
	//session_start();

	class Main_controller extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model("expense_model");
			$this->load->helper('url');
			$this->load->library('session');

		}
		function index(){
			
			$this->load->view("login-to-expensemanager");
			//$this->load->view("expense-mainpage");
			//$this->showAllData();
			//$this->load->model("expense_model");
		}

		public function login_to_expensemanager(){
			// header('Content-type: application/json');
   			//header('Access-Control-Allow-Origin: *');
			$data = array(
				'email' =>$this->input->post('email'),
				'password'=>$this->input->post('pass')
			);
			//var_dump($data);
			$result=$this->expense_model->validateUser($data);
			//var_dump($result);
			if ($result !=false){
				//echo $result[0];
				$session_data = array(
				'name'=>$result[0]->name,
				'address'=>$result[0]->address,
				'email' => $result[0]->email,
				'password' => $result[0]->password
				 );
				//var_dump($session_data);
				//$jsonData=$this->getUserInfo($session_data);
				$this->session->set_userdata($session_data);
				$this->showAllData();
				//return $jsonData;

			}

			else{
				// $data = array('msg' => 'Invalid username or password' );
				$data['msg']='Invalid user and password';
				$this->load->view('login-to-expensemanager',$data);
			}
			
		}
  
		public function getUserInfo($data){
			//echo "hello";
			//header('Content-type:application/json');
   			header('Access-Control-Allow-Origin: *');
   			//$arrayData=$this->login_to_expensemanager();
   			//$jsonString="{";
   			//var_dump($data);
   			return json_encode($data);	 
		}

		public function logoutUser(){
			$this->session->sess_destroy();
			$this->index();
		}

		public function insertRegisteredData(){
			header('Content-type: application/json');
        	header('Access-Control-Allow-Origin: *');
			$data['name']=$this->input->post('name');
			$data['address']=$this->input->post('address');
			$data['email']=$this->input->post('email');
			$data['password']=$this->input->post('pass');
			$res=$this->expense_model->insertRegisteredDataTodb($data);
			echo json_encode($res);

		}
		 
		 
		public function insertExpensesData(){

			
			$data['date']=$this->input->post('dateE');
			$data['description']=$this->input->post('descriptionE');
			$data['amount']=$this->input->post('amountE');
			$data['category']=$this->input->post('categoryE');

			$res=$this->expense_model->insertExpensesDataToDb($data);

			if($res){

				header('location:'.base_url()."index.php/Main_controller/".$this->index());

			}
		}

		public function insertIncomeData(){

			$data['date']=$this->input->post('dateI');
			$data['description']=$this->input->post('descriptionI');
			$data['amount']=$this->input->post('amountI');
			$data['category']=$this->input->post('categoryI');

			$res=$this->expense_model->insertIncomeDataToDb($data);

			if($res){

				header('location:'.base_url()."index.php/Main_controller/".$this->index());

			}

		}

		public function updateIncomeData(){
			$id=$this->input->post('idIncome');
			$data['date']=$this->input->post('editIncomeDate');
			$data['description']=$this->input->post('descriptionIncome');
			$data['amount']=$this->input->post('amountIncome');
			$data['category']=$this->input->post('categoryIncome');

			$res=$this->expense_model->updateIncome($data,$id);
			echo json_encode($res);

		}

		public function showAllData(){
			$data["records"]=$this->expense_model->getAllData();
			//echo json_encode($data);
			$this->load->view("expense-mainpage",$data);
		}

		public function showAllIncomeData(){
			header('Content-type: application/json');
        	header('Access-Control-Allow-Origin: *');
			$data["records"]=$this->expense_model->getAllIncomeData();
			echo json_encode($data);
			//$this->load->view("expense-mainpage",$data);
		}


		public function delete(){
			$this->load->helper('url');
			$id = $this->uri->segment(3);
			
			if(empty($id)){
				show_404();
			}
			$this->expense_model->deleteData($id);
			redirect(site_url().'/Main_controller', 'refresh');
  		}

  		public function deleteIncome($id){
  			 header('Content-type: application/json');
     		 header('Access-Control-Allow-Origin: *');
  			$data=$this->expense_model->deleteIncomeData($id);
  			echo json_encode($data);
  		}

		public function edit($id){

			//$id=$this->uri->segment(3);
			header('Content-type: application/json');
        	header('Access-Control-Allow-Origin: *');
			$data["data"]=$this->expense_model->getDataById($id);
			echo json_encode($data);

		}

		public function editIncome($id){

			//$id=$this->uri->segment(3);
			header('Content-type: application/json');
        	header('Access-Control-Allow-Origin: *');
			$data["data"]=$this->expense_model->getIncomeDataById($id);
			echo json_encode($data);

		}

		public function update(){

			$id=$this->input->post('id');
			$data['date']=$this->input->post('date');
			$data['description']=$this->input->post('description');
			$data['amount']=$this->input->post('amount');
			$data['category']=$this->input->post('category');

			$res=$this->expense_model->updateData($data,$id);

			if($res){
				header('location:'.base_url()."index.php/Main_controller/".$this->index());
			}

		}

		public function insertExpenseCategory(){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			$data['category_name']=$this->input->post('expenseCategoryName');
			$res=$this->expense_model->insertExpensesCategoryToDB($data);
			echo json_encode($res);
		}

		public function showAllExpenseCategory(){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			//echo "showAllExpenseCategory function";
			$data['record']=$this->expense_model->getAllExpensesCategory();
			echo json_encode($data);
		}

		public function deleteExpenseCategory($id){
  			 header('Content-type: application/json');
     		 header('Access-Control-Allow-Origin: *');
  			$data=$this->expense_model->deleteExpenseCategoryData($id);
  			echo json_encode($data);
  		}

		public function editExpenseCategory($id){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			$data['result']=$this->expense_model->getExpenseCategoryById($id);
			echo json_encode($data);
		}

		public function updateExpenseCategory(){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			$id=$this->input->post('idEcategory');
			$data['category_name']=$this->input->post('editEcategoryName');
			$res=$this->expense_model->updateExpenseCategoryData($data,$id);
			echo json_encode($res);
		}

		public function insertIncomeCategory(){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			$data['category_name']=$this->input->post('incomeCategoryName');
			// $data="test";
			$res=$this->expense_model->insertIncomeCategoryToDB($data);
			echo json_encode($res);
		}

		public function showAllIncomeCategory(){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			//echo "showAllExpenseCategory function";
			$data['record']=$this->expense_model->getAllIncomeCategory();
			echo json_encode($data);
		}


		public function deleteIncomeCategory($id){
  			 header('Content-type: application/json');
     		 header('Access-Control-Allow-Origin: *');
  			$data=$this->expense_model->deleteIncomeCategoryData($id);
  			echo json_encode($data);
  		}

  		public function editIncomeCategory($id){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			$data['result']=$this->expense_model->getIncomeCategoryById($id);
			echo json_encode($data);
		}

		public function updateIncomeCategory(){
			header('Content-type: application/json');
   			header('Access-Control-Allow-Origin: *');
			$id=$this->input->post('idIcategory');
			$data['category_name']=$this->input->post('editIcategoryName');
			
			$res=$this->expense_model->updateIncomeCategoryData($data,$id);
			echo json_encode($res);
		}


	}

	
 ?>