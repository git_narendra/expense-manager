<?php 

	/**
	* 
	*/
	class Login_controller extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->helper("url");
			
		}

		public function index(){
			$this->load->view('login');
		}

		public function signupUser(){
			$this->load->view('signup-user');
		}

		public function insert_data(){
			$this->load->model('user_model');
			$data['first-name']=$this->input->post('fname');
			$data['last-name']=$this->input->post('lname');
			$data['email']=$this->input->post('email');
			$data['password']=$this->input->post('pass');

			$res=$this->user_model->insert_data_db($data);

			if ($res) {
				header('location:'.base_url()."index.php/Login_controller/".$this->index());

			}
		}

		public function login_user(){
			$this->load->model("user_model");
			$uname=$this->input->post("uname");
			$pass=$this->input->post("pwd");

			if ($uname && $pass && $this->user_model->validate_user($uname,$pass)) {
				redirect('MainPageController/show_main');
			}

			else{
				$this->show_login(true);
			}
		}

		public function show_login($show_error=false){

			$data['error']= $show_error;
			$this->load->helper('form');
			$this->load->view('login',$data);

		}

		public function logout_user(){
			$this->load->library('session');
			$this->session->sess_destroy();
			$this->index();
		}
	}
 ?>