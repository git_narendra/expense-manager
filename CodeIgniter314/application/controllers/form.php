<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	public function mainPage()
	{
		
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('content');
		$this->load->view('footer');
	}

}

/* End of file form.php */
/* Location: ./application/controllers/form.php */