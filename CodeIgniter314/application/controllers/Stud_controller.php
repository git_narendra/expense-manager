<?php 
	/**
	* 
	*/
	class Stud_controller extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->helper("url");
			$this->load->model("Stud_model");
		}

		public function index(){
			$data["Studdata"]=$this->Stud_model->get_all_data();
			$this->load->view("Stud_views",$data);
		}

		public function add_form(){
			$this->load->view("insert");
		}

		public function insert_data(){
			
			$udata["name"]=$this->input->post("name");
			$udata["address"]=$this->input->post("address");
			$udata["colz_name"]=$this->input->post("colz_name");

			$res=$this->Stud_model->insert_data_to_db($udata);

			if($res){

			header('location:'.base_url()."index.php/Stud_controller/".$this->index());

			}
		}

		public function update(){

			$udata["name"]=$this->input->post("name");
			$udata["address"]=$this->input->post("address");
			$udata["colz_name"]=$this->input->post("colz_name");

			$res=$this->Stud_model->update_info($udata,$_POST['id']);

			if($res){

			header('location:'.base_url()."index.php/Stud_controller/".$this->index());

			}

		}


		public function edit(){

			$id = $this->uri->segment(3);
		
			$data['stud'] = $this->Stud_model->getDataById($id);
		
			$this->load->view('edit', $data);
		
		}


		public function delete($id){
			$this->Stud_model->delete_data($id);
			$this->index();
		}


	}

 ?>