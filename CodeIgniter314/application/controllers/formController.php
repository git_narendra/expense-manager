<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FormController extends CI_Controller {

	public function _constructor(){
		parent::__construct();
		;
		$this->load->helper('url_helper');
	}

	public function index()
	{
		
		$this->load->helper('url');
		$this->load->view('personRegisterForm');
		

	}

	function saveData(){
		$this->load->model('main_model')
		$this->main_model->insertData();
		redirect("formController/index",'refresh');
	}
}

/* End of file formController.php */
/* Location: ./application/controllers/formController.php */