<?php 
	/**
	* 
	*/
	class Stud_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database("test_db");
		}

		public function get_all_data(){
			$query=$this->db->get('stud');
			return $query->result();
		}

		public function getDataById($id){
			$query=$this->db->get_where("stud",array("id"=>$id));
			return $query->row_array();
		}


		public function insert_data_to_db($data){
			return $this->db->insert('stud',$data);
		}

		public function update_info($data,$id){
			$this->db->where('stud.id',$id);
			return $this->db->update('stud',$data);
		}

		public function delete_data($id){
			$this->db->where("stud.id",$id);
			return $this->db->delete('stud');
		}
	}


 ?>