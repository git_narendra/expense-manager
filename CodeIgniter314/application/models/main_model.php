<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

	function _constructor(){

		parent::__construct();
	}

	public function insertData(){
		$data = array('name' =>$this->input->post('name'),'address' =>$this->input->post('address'),
			'gender' =>$this->input->post('gender'));
		$this->db->insert('person_info',$data);
	}
	
	public function fetchTable(){
		$query=$this->db->get('person_info');
		return $query->result();
	}

}

/* End of file main_model.php */
/* Location: ./application/models/main_model.php */