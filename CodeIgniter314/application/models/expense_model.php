<?php 

	/**
	* 
	*/
	class expense_model extends CI_Model
	{
		function __construct(){
			parent::__construct();
		}

		public function validateUser($data){
			// $this->db->select('*');
			// $this->db->from('user_info');
			// $this->db->where('name',$data['username']);
			// $result=$this->db->get()->result();

			$sql="SELECT * FROM user_info WHERE email=? AND password=?";
			$query=$this->db->query($sql,$data);
			$result=$query->result();
			//var_dump($result);
			if (is_array($result) && count($result)==1) {
				return $result;

			}

			return false;

		}

		public function insertRegisteredDataTodb($data){
			return $this->db->insert('user_info',$data);
		}

		public function getRegisteredDataFromDb(){
			$query=$this->db->get();
		}

		public function insertExpensesDataToDb($data){
			return $this->db->insert('expenses',$data);
		}


		public function insertIncomeDataToDb($data){
			return $this->db->insert('income',$data);
		}


		public function getAllData(){
			$query = $this->db->get("expenses");
			return $query->result();
		
		}

		public function getAllIncomeData(){
			$query = $this->db->get("income");
			return $query->result();
		
		}

		public function deleteData($id){
			$this->db->where("expenses.id",$id);
			return $this->db->delete("expenses");

		}

		public function deleteIncomeData($id){
			$this->db->where("income.id",$id);
			return $this->db->delete("income");
		}

		public function getDataById($id){
			$query=$this->db->get_where("expenses",array('id' =>$id));
			return $query->row_array();
		}

		public function getIncomeDataById($id){
			$query=$this->db->get_where("income",array('id' =>$id));
			return $query->row_array();
		}

		public function updateIncome($data,$id){
			$this->db->where('income.id',$id);
			return $this->db->update('income',$data);
		}

		public function updateData($data,$id){
			/*$this->db->set($data);
			$this->db->where('id',$id);
			$this->db->update('expenses',$data);*/

			$this->db->where('expenses.id',$id);
			return $this->db->update('expenses',$data);
		}

		public function insertExpensesCategoryToDB($data){
			//return $this->db->insert('income',$data);
			return $this->db->insert('expense_category',$data);
		}

		public function deleteExpenseCategoryData($id){
			$this->db->where("expense_category.id",$id);
			return $this->db->delete("expense_category");
		}

		public function getAllExpensesCategory(){
			$query=$this->db->get("expense_category");
			return $query->result();
		}

		public function getExpenseCategoryById($id){
			$query=$this->db->get_where("expense_category",array('id' =>$id));
			return $query->row_array();
		}

		public function updateExpenseCategoryData($data,$id){
			$this->db->where("expense_category.id",$id);
			return $this->db->update("expense_category",$data);
		}

		public function insertIncomeCategoryToDB($data){
			//return $this->db->insert('income',$data);
			return $this->db->insert('income_category',$data);
		}

		public function getAllIncomeCategory(){
			$query=$this->db->get("income_category");
			return $query->result();
		}

		public function deleteIncomeCategoryData($id){
			$this->db->where("income_category.id",$id);
			return $this->db->delete("income_category");
		}

		public function getIncomeCategoryById($id){
			$query=$this->db->get_where("income_category",array('id' =>$id));
			return $query->row_array();
		}

		public function updateIncomeCategoryData($data,$id){
			
			$this->db->where('income_category.id',$id);
			return $this->db->update('income_category',$data);
		}

	}
 ?>