<?php 
	/**
	* 
	*/
	class user_model extends CI_Model
	{
		var $details;
		function __construct()
		{
			parent::__construct();
			$this->load->database("test_db");
		
		}

		function insert_data_db($data){
			return $this->db->insert('user_info',$data);
		}

		function validate_user($email,$passwd){

			$this->db->from('user_info');
			$this->db->where('email',$email);
			$this->db->where('password',$passwd);
			$login=$this->db->get()->result();

			if(is_array($login) && count($login)==1){

				$this->details=$login[0];
				$this->set_session();

				return true;

			}

			return false;

		}


		function set_session(){
			$this->load->library('session');
			$this->session->set_userdata(array(
				'id'=>$this->details->id,
				'first-name'=>$this->details->fname,
				'last-name'=>$this->details->lname,
				'email'=>$this->details->email,
				'password'=>$this->details->pass,
				'isLoggedIn'=>true
				));

		}
	}

 ?>